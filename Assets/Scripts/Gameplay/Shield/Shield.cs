using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
	public CircleCollider2D Collider;


    	void OnTrrigerEnter2D(Collider2D collision) {
		if (collision.gameObject.tag == "Object") {
            collision.gameObject.tag = "Shielded";
			AudioManager.instance.PlayRandomSfx("SD_ShieldHold");
			}
        else if (collision.gameObject.tag == "Attack"){
			AudioManager.instance.PlayRandomSfx("ShieldRelease");
			Destroy(collision.gameObject);
        }
        else
        {
			Debug.Log("Did i touched something ?");            
        }
		}

        void OnTriggerExit2D(Collider2D collision) {
		if (collision.gameObject.tag == "Shielded") {
            collision.gameObject.tag = "Object";
			}
		}
}
